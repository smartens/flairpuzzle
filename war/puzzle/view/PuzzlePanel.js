Ext.define('Puzzle.view.PuzzlePanel', {
	extend : 'Ext.form.Panel',
	requires:['Puzzle.view.PuzzleMatrix'],
	alias : 'widget.puzzlePanel',
	id:'puzzlePanel',
	monitorValid:true,
	autoScroll:true,
	defaults : {
		padding : 5
	},
	style: {margin: '30px'},
	tbar:[{xtype:'numberfield',allowBlank:false,width:160,labelWidth:30,fieldLabel:'Dim.',id:'dimension',
			listeners:{
				/*blur:function(field){
					//Todo check for empty value
					Ext.getCmp('content').minLength=field.getValue()*field.getValue();
					Ext.getCmp('content').maxLength=field.getValue()*field.getValue();
				},*/
				change:function(field){
					Ext.getCmp('content').minLength=field.getValue()*field.getValue();
					Ext.getCmp('content').maxLength=field.getValue()*field.getValue();
				}
			},
	      },
			{xtype:'textfield',allowBlank:false,width:300,labelWidth:40,fieldLabel:'Content',id:'content'},
			{xtype:'button',formBind:true,text:'Create puzzle', handler:function(){
				Ext.getCmp('findButton').enable();
				Ext.getCmp('puzzlePanel').removeAll();
				var puzzle=Ext.getCmp('content').getValue();
				var dimension=Ext.getCmp('dimension').getValue();
				childItems = [];
				for (i = 0; i < dimension * dimension; ++i) {
					childItems.push({
						xtype: 'container',
						width: 50,
						height: 50,
						html: '<div style="position:relative;top:15px">'+puzzle.charAt(i)+'</div>',
						style: {borderColor:'#000000','text-align':'center', borderStyle:'none', borderWidth:'1px'}
					});
				}
				Ext.getCmp('puzzlePanel').add({
					xtype:'panel',
					layout: {
						type: 'table',
						columns: dimension
					},
					items:childItems
				})
			}},
			{xtype:'button', text:'Example 7 by 7',handler:function(){
				Ext.getCmp('dimension').setValue(7);
				Ext.getCmp('content').setValue('FYYHNRDRLJCINUAAWAAHRNTKLPNECILFSAPEOGOTPNHPOLAND');
			}},
			{xtype:'button', text:'Example 9 by 9',handler:function(){
				Ext.getCmp('dimension').setValue(9);
				Ext.getCmp('content').setValue('OXZOCFKOGBOTSWANAEMGBFRNWSRBDOEWAUPMGTWYLDYAAJVEQAGYDNKRPNYDILYAILARTSUAYHZJUZLUM');
			}}
			
			
	],
	bbar:[
		{xtype:'textfield',id:'word'},
		{xtype:'button',
			text:'Find word',
			disabled:true,
			action:'findWord',
			id:'findButton',
			handler:function(){
				var dimension=Ext.getCmp('dimension').getValue();
				for (var i=0;i<dimension*dimension;i++){
					Ext.getCmp('puzzlePanel').items.items[0].items.items[i].removeCls('yellow-background');
				}
				Ext.Ajax.request({
				    url: 'rest/puzzle',
				    method:'GET',
				    params: {
				        puzzle:Ext.getCmp('content').getValue(),
				        word:Ext.getCmp('word').getValue().toUpperCase()
				    },
				    success: function(response){
				        var text = response.responseText;
				        var letters= Ext.JSON.decode(text).data;
				        if (letters.length==0){
				            Ext.example.msg('Not found', Ext.getCmp('word').getValue()+' could not be found', Ext.getCmp('findButton'));

				        }
				        for (var i=0;i<letters.length;i++){
							Ext.getCmp('puzzlePanel').items.items[0].items.items[letters[i]].addCls('yellow-background');
						}
				    }
				});
			}
		}
	],
    items : [
	]
});