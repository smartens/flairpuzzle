Ext.define('Puzzle.view.Viewport', {
	extend : 'Ext.container.Viewport',
	requires : [ 'Puzzle.view.PuzzlePanel'],
	autoScroll : false,
	initComponent : function() {
		this.items = {
			xtype : 'panel',
			width : 1350,
			height: 600,
			layout : 'card',
			tbar:[//{xtype:'image',src:'resources/img/inbologo.png',style:'top:1px !important;height:22px'},,
			      {xtype:'label', text: 'Flair puzzle', cls:'x-panel-header-text-container-default title-label'}
			      //,
        	      //{xtype:'tbfill'},
        	      //{xtype:'button',action: 'logout',icon: 'resources/img/door_out.png',text: 'Afmelden'}
			],
        	
			items : [ 
			      {xtype : 'puzzlePanel'}
			      //{xtype : 'tripPanel'},
			      //{xtype : 'observationPanel'}
			]
		};
		this.callParent();
	}
});