Ext.application({
	name : 'Puzzle',
	appFolder : 'puzzle',
	autoCreateViewport : true,
	controllers : [ 'Puzzle'],
	launch : function() {
		if (!window.console) { 
			console = {
				log: function(){}, 
				warn: function(){}, 
				error: function(){} 
			};
		};
	}
});