import java.util.List;

import org.junit.Test;

import be.flair.puzzle.service.impl.PuzzleServiceImpl;
import static org.junit.Assert.assertEquals;

public class PuzzleServiceTest {

	@Test
    public void testFindWord() {
		PuzzleServiceImpl service=new PuzzleServiceImpl();
		String puzzle="FYYHNRDRLJCINUAAWAAHRNTKLPNECILFSAPEOGOTPNHPOLAND";
		String word="JAPAN";
		List<Integer> result=service.findWord(puzzle, word);
		assertEquals(9,result.get(0).intValue());
		assertEquals(17,result.get(1).intValue());
		assertEquals(25,result.get(2).intValue());
		assertEquals(33,result.get(3).intValue());
		assertEquals(41,result.get(4).intValue());
    }
}
