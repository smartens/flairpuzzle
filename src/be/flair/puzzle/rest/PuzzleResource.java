package be.flair.puzzle.rest;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import be.flair.puzzle.service.PuzzleService;

/**
 * A rest api for handling all puzzle calls
 *
 */
@Component
@Path("puzzle")
public class PuzzleResource extends ResourceBase {

	@Autowired
	private PuzzleService puzzleService;

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getWordIndices(@QueryParam("puzzle") String puzzle,
			@QueryParam("word") String word) throws JsonParseException, JsonMappingException, IOException {
		List<Integer> letterIndices=puzzleService.findWord(puzzle, word);
		return buildSuccessfulResponse(letterIndices);	
	}

}
