package be.flair.puzzle.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import be.flair.puzzle.service.PuzzleService;

@Service
public class PuzzleServiceImpl implements PuzzleService {

	@Override
	public List<Integer> findWord(String puzzle, String word) {
		if (StringUtils.isEmpty(word)){
			return new ArrayList<Integer>();
		}
		return getWord(puzzle.toUpperCase(),word.toUpperCase());
	}
	
	/**
	 * Gets the word from the puzzle.
	 * 
	 * @param puzzle a string representation of the puzzle
	 * @param word the word to look for
	 * @return a list in indices of all the letters of the word
	 */
	private List<Integer> getWord(String puzzle, String word) {
		List<Integer> result=new ArrayList<Integer>();
		Integer dimension = new Double(Math.sqrt(puzzle.length())).intValue();
		if (dimension * dimension != puzzle.length()) {
			throw new RuntimeException("the puzzle is not a square");
		}
		//first we will try to find the first letter
		char firstLetter = word.charAt(0);
		Integer direction=0;
		Integer i = 0;
		while (direction==0 && puzzle.indexOf(firstLetter, i) >= 0) {
			System.out.println("found beginletter at "
					+ puzzle.indexOf(firstLetter, i));
			//see if we can find the word starting from this letter
			direction = searchWordFrom(puzzle, word,
					puzzle.indexOf(firstLetter, i));
			i = puzzle.indexOf(firstLetter, i) + 1;
		}
		Integer beginIndex=i-1;
		if (direction != 0 && beginIndex>-1){
			for (int j=0;j<word.length();j++){
				result.add(beginIndex+j*direction);
			}
		}
		return result;
	}

	/**
	 * Search the word starting from a specific first letter within the puzzle
	 * @param puzzle a string representation for the puzzle
	 * @param word the word to look for
	 * @param begin the index of the letter we start searching from
	 * @return the direction set as an integer (the index step to take to switch to the next letter) 
	 */
	private Integer searchWordFrom(String puzzle, String word, Integer begin) {
		System.out.println("searching word from index " + begin);
		Integer dimension = new Double(Math.sqrt(puzzle.length())).intValue();
		//look left
		if (searchWordFromAndDirection(puzzle, word, begin, -1)) {
			return -1;
		}
		//look right
		if (searchWordFromAndDirection(puzzle, word, begin, 1)) {
			return 1;
		}
		//look up
		if (searchWordFromAndDirection(puzzle, word, begin, -dimension)) {
			return -dimension;
		}
		//look down
		if (searchWordFromAndDirection(puzzle, word, begin, dimension)) {
			return dimension;
		}
		//look upper left
		if (searchWordFromAndDirection(puzzle, word, begin, -dimension - 1)) {
			return -dimension - 1;
		}
		//look upper right
		if (searchWordFromAndDirection(puzzle, word, begin, -dimension + 1)) {
			return -dimension + 1;
		}
		//look lower left
		if (searchWordFromAndDirection(puzzle, word, begin, dimension - 1)) {
			return dimension - 1;
		}
		//look lower right
		if (searchWordFromAndDirection(puzzle, word, begin, dimension + 1)) {
			return dimension + 1;
		}

		return 0;
	}

	/**
	 * Search the word from a given begin point in a certain given direction
	 * @param puzzle a string representation for the puzzle
	 * @param word the word to look for
	 * @param begin the index of the letter we start searching from
	 * @param direction the direction set as an integer (the index step to take to switch to the next letter) 
	 * @return true if found, false otherwise
	 */
	private boolean searchWordFromAndDirection(String puzzle, String word,
			Integer begin, Integer direction) {
		String searchWord = word.substring(1);
		Integer index = begin + direction;
		if (index < 0 || index >= puzzle.length()) {
			//we have hit the boundaries of the puzzle
			return false;
		}
		while (index > -1 && index < puzzle.length()) {
			if (searchWord.length() == 0) {
				//the word has been found
				return true;
			}
			if (searchWord.charAt(0) == puzzle.charAt(index)) {
				searchWord = searchWord.substring(1);
				index = index + direction;
				if (searchWord.length() == 0) {
					//the word has been found
					return true;
				}
			} else {
				//the next letter does not match
				return false;
			}
		}
		//we have hit the boundaries of the puzzle
		return false;
	}
}