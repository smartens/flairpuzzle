package be.flair.puzzle.service;

import java.util.List;

/**
 * A service interface describing all puzzle methods
 *
 */
public interface PuzzleService {

	/**
	 * Find a word in a given puzzle.
	 * @param puzzle a string representation of the puzzle.
	 * @param word the word to look for
	 * @return a list of indices where to find the word
	 */
	List<Integer> findWord(String puzzle, String word);

}
